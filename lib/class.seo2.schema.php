<?php

if (!isset($gCms)) exit;

function getUrl() {

    if(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off'|| $_SERVER['SERVER_PORT'] == 443){
        return 'https://'.$_SERVER['HTTP_HOST'];
    }
    return 'http://'.$_SERVER['HTTP_HOST'];
}


function getTypeWebsite($mod)
{
    $websiteSchema = (object)[
        "@context" => "http://schema.org",
        "@type" => "WebSite",
        "name" => $mod->GetPreference('schema_sitename'),
        "url" => getUrl()
    ];
    return "<script type='application/ld+json'>" . json_encode($websiteSchema) . "</script>";
}

function getAddressType($mod)
{
    $addressSchmea = (object)[
        "@context" => "http://schema.org",
        "@type" => "Organization",
        "name" => $mod->GetPreference('schema_sitename'),
        "email" => $mod->GetPreference('schema_email'),
        "address" => (object)[
            "@type" => "PostalAddress",
            "addressLocality" => $mod->GetPreference('schema_city'),
            "addressRegion" => $mod->GetPreference('schema_state'),
            "postalCode" => $mod->GetPreference('schema_zipcode'),
            "addressCountry" => $mod->GetPreference('schema_country'),
            "streetAddress" => $mod->GetPreference('schema_street'),
        ],
        "telephone" => $mod->GetPreference('schema_phone'),
        "faxNumber" => $mod->GetPreference('schema_fax')
    ];
    return "<script type='application/ld+json'>" . json_encode($addressSchmea) . "</script>";
}

function getLogoType($mod)
{

    $logoSchema = (object)[
        "@context" => "http://schema.org",
        "@type" => "Organization",
        "logo" => $mod->GetPreference('schema_logo'),
        "url" => getUrl()
    ];
    return "<script type='application/ld+json'>" . json_encode($logoSchema) . "</script>";
}

function getSocialLinks($mod)
{
    $linkString = str_replace(array('\r', '\n'), '', json_encode($mod->GetPreference('schema_social')));
    $socialLinks = (object)[
        "@context" => "http://schema.org",
        "@type" => "Organization",
        "url" => getUrl(),
        "sameAs" => explode(',', json_decode($linkString))
    ];
    return "<script type='application/ld+json'>" . json_encode($socialLinks) . "</script>";
}

function schemaBreadcrumbs()
{
    $smarty = cmsms()->GetSmarty();
    $templateVars = $smarty->get_template_vars();
    $contentObj = $templateVars['content_obj'];
    $pathArray = explode('/', $contentObj->HierarchyPath());

    $breadCrumbs = (object)[
        "@context" => "http://schema.org",
        "@type" => "BreadcrumbList",
        "itemListElement" => []
    ];

    foreach ($pathArray as $index => $value){
        $item = (object)[
            "@type" => "ListItem",
            "position" => $index,
            "item" => (object)[
                "name" => $value
            ]
        ];
        array_push($breadCrumbs->itemListElement, $item);
    }
    return "<script type='application/ld+json'>" . json_encode($breadCrumbs) . "</script>";

}

function generateSchemaHeadTag($mod, $content)
{
    $schemaArray = [
        getAddressType($mod),
        getTypeWebsite($mod),
        getLogoType($mod),
        getSocialLinks($mod),
        schemaBreadcrumbs()
    ];
    return   $content = str_replace('</head>', implode('', $schemaArray) . '</head>', $content);
}