<?php
#-------------------------------------------------------------------------
# Module: SEOTools2 - Turbo-charge your on-page SEO
# Version: 1.2, Clip Magic <admin@clipmagic.com.au>
# Tab: Sitemap Settings
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/skeleton/
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
if (!isset($gCms)) exit;

$schmeasettings = $this->CreateFormStart(null, 'changesettings');
$schmeasettings .= $this->CreateFieldsetStart(null, 'title_schmea_description', $this->Lang('title_schmea_description'));

$schmeasettings .= '<p class="pagetext">'.$this->lang('schema_logo').':</p>';
$schmeasettings .= '<p class="pageinput">'  .$this->CreateInputTel(null, 'schema_logo', $this->GetPreference('schema_logo', ''), 70) . ' '  . '</p>';


$schmeasettings .= '<p class="pagetext">'.$this->lang('schema_sitename').':</p>';
$schmeasettings .= '<p class="pageinput">'  .$this->CreateInputText(null, 'schema_sitename', $this->GetPreference('schema_sitename', ''), 70) . ' '  . '</p>';

$schmeasettings .= '<p class="pagetext">'.$this->lang('schema_street').':</p>';
$schmeasettings .= '<p class="pageinput">' .$this->CreateInputText(null, 'schema_street', $this->GetPreference('schema_street', ''), 70) . ' '  . '</p>';

$schmeasettings .= '<p class="pagetext">'.$this->lang('schema_city').':</p>';
$schmeasettings .= '<p class="pageinput">' .$this->CreateInputText(null, 'schema_city', $this->GetPreference('schema_city', ''), 70) . ' '  . '</p>';

$schmeasettings .= '<p class="pagetext">'.$this->lang('schema_state').':</p>';
$schmeasettings .= '<p class="pageinput">'  .$this->CreateInputText(null, 'schema_state', $this->GetPreference('schema_state', ''), 70) . ' '  . '</p>';

$schmeasettings .= '<p class="pagetext">'.$this->lang('schema_zipcode').':</p>';
$schmeasettings .= '<p class="pageinput">'  .$this->CreateInputText(null, 'schema_zipcode', $this->GetPreference('schema_zipcode', ''), 70) . ' '  . '</p>';

$schmeasettings .= '<p class="pagetext">'.$this->lang('schema_country').':</p>';
$schmeasettings .= '<p class="pageinput">'  .$this->CreateInputText(null, 'schema_country', $this->GetPreference('schema_country', ''), 70) . ' '  . '</p>';

$schmeasettings .= '<p class="pagetext">'.$this->lang('schema_phone').':</p>';
$schmeasettings .= '<p class="pageinput">'  .$this->CreateInputText(null, 'schema_phone', $this->GetPreference('schema_phone', ''), 70) . ' '  . '</p>';

$schmeasettings .= '<p class="pagetext">'.$this->lang('schema_fax').':</p>';
$schmeasettings .= '<p class="pageinput">'  .$this->CreateInputTel(null, 'schema_fax', $this->GetPreference('schema_fax', ''), 70) . ' '  . '</p>';

$schmeasettings .= '<p class="pagetext">'.$this->lang('schema_social').':</p>';
$schmeasettings .= '<p class="pageinput">'  .$this->CreateTextArea(false, 'schema_social', $this->GetPreference('schema_social'), '') . '</p>';
$schmeasettings .= '<p>'.$this->lang('schmea_social_help').'</p>';


$schmeasettings .= $this->CreateFieldsetEnd();
$schmeasettings .= '<p class="pageinput">' . $this->CreateInputSubmit(null, 'save_schema_settings', $this->lang('save')) . '</p>';

$schmeasettings .= $this->CreateFormEnd();

echo $schmeasettings;


?>